### Samples of my work

If you're logged in to GitLab, you should be able to see several of [my other projects](https://gitlab.com/users/aayore/projects).

I'd like to specifically direct your attention to the [Intro to DevOps class](https://gitlab.com/aayore/introtodevops) I created.  Students have given me great feedback, and it's been a lot of fun to teach.
