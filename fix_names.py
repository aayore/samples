#!/usr/bin/env python3

import os, re, argparse

# Initialize a dictionary we'll use to store file/type metadata for later
types = {}

# Parse command line arguments (could use sys.argv, but I think this is
# more extensible, and provides help from the CLI).
parser = argparse.ArgumentParser(description='Rename files sequentially.')
parser.add_argument('-y','--yes', action='store_true', help='Answer "yes" to all prompts.')
parser.add_argument('-q','--quiet', action='store_true', help='Silence unnecessary output.')
parser.add_argument('path', help='The path containing your files.')
args = parser.parse_args()

# Cute (I hope) hack to facilitate loud/quiet  :)
say = not args.quiet

# Make sure we received a legitimate path
try:
    # Make sure we have a trailing slash (we'll need it later)
    if not args.path.endswith('/'):
        args.path += '/'
    files = os.listdir(args.path)
except:
    raise SystemExit('ERROR: Unable to list files in {}'.format(args.path))

# Build a one-to-many mapping of filetypes to files
for f in files:
    # For a file like foo_XYZ.bar, we create a filetype of foo.bar
    # that we can use to associate all foo_*.bar files
    filetype = ''.join(re.search(r'(.*)_\d*(\..*)',f).group(1,2))
    # Append our file to the dictionary if the type has already been found
    if filetype in types:
        types[filetype].append(f)
    # Create a new mapping if this is the first time we've encountered
    # this particular type
    else:
        types[filetype] = [f]

if not args.yes:
    # Check with the user, since we're modifying files on the file system
    print('Sequentially renaming all files in {}'.format(args.path.rstrip('/')))
    ans = input('Are you sure? (y/N) ')
    if 'y' not in ans.lower():
        raise SystemExit('Aborted.')

# Rename the files
for filetype in types:
    # Split our type mapping to use for naming
    # Performing this join in order to restore any '.' to the file name
    name = '.'.join(filetype.split('.')[:-1])
    extension = filetype.split('.')[-1]
    # Reset the sequence so each type starts at 01
    i = 1
    # Zero-pad the sequence number (remove the +1 if you prefer)
    width = len(str(len(types[filetype]))) + 1
    # Move all files to their temporary, sorted, sequential names
    for f in sorted(types[filetype]):
        old = args.path + f
        new = args.path + name + '_{:0{w}}.'.format(i,w=width) + extension
        print(say * 'Renaming {} to {}...'.format(old,new),end='\n' if say else '')
        os.rename(old,new)
        # Increment the sequence number for each file
        i += 1

print(say * 'Complete!',end='\n' if say else '')
