{
  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "Mesos Cluster ({{stack_version}})",

  "Metadata" : {
    "Comments" : {
      "HardCoding" : [
        "Some values (specifically those in the RegionMap) are hard-coded with values from the persistent",
        "VPC configurations in us-east-1 and us-west-2."
    ] }
  },

  "Parameters" : {

    "aaagreement" : {
      "Type" : "String",
      "AllowedValues" : [ "Yes" ],
      "Description" : "Do you agree to name this stack mesos-{env}{iteration}-{accessMode}?  (e.g. mesos-dev1-priv)"
    },

    "AccessMode" : {
      "Type" : "String",
      "AllowedValues" : [ "public", "private" ],
      "Default" : "private",
      "Description" : "Will this cluster run public or private apps?"
    },

    "Environment" : {
      "Type" : "String",
      "AllowedValues": [ "Development", "QA", "UAT", "Production" ],
      "Default" : "Development"
    },

    "AdminIP" : {
      "Type" : "String",
      "Description" : "CIDR range of IP addresses that will be used to administer the cluster",
      "Default" : "12.10.190.123/32"
    },

    "ChronosImage" : {
      "Type" : "String",
      "Description" : "Chronos image to use",
      "Default" : "mesosphere/chronos:v3.0.2",
      "AllowedValues" : [
        "mesosphere/chronos:v3.0.2",
        "mesosphere/chronos:fix-for-772"
      ]
    },

    "Domain" : {
      "Type" : "String",
      "Description" : "DNS Domain for this cluster (mesos.east will only work in N. Virginia; mesos.west will only work in Oregon)",
      "Default" : "my.domain",
      "AllowedValues" : [ "my.domain" ]
    },

    "SshKey" : {
      "Type" : "String",
      "Description" : "The SSH key that will be used to access instances of this cluster",
      "Default" : "ProdOps2016",
      "AllowedValues" : [ "ProdOps2016" ]
    },

    "InstanceQtyMaster" : {
      "Type" : "String",
      "Description" : "Master node configuration for this cluster (Multi==3)",
      "Default" : "Single",
      "AllowedValues" : [ "Single", "Multi" ]
    },

    "InstanceTypeMaster" : {
      "Type" : "String",
      "Description" : "Will be used for Mesos master nodes (which also run Zookeeper and Marathon)",
      "AllowedValues" : [
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m4.large"
      ],
      "Default" : "t2.small"
    },

    "InstanceTypeAgent" : {
      "Type" : "String",
      "Description" : "Will be used for Mesos agent nodes",
      "AllowedValues" : [
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge",
        "m3.medium",
        "m3.large",
        "m3.xlarge",
        "m3.2xlarge",
        "c4.large",
        "c4.xlarge",
        "c4.2xlarge",
        "c4.4xlarge",
        "c4.8xlarge",
        "c3.large",
        "c3.xlarge",
        "c3.2xlarge",
        "c3.4xlarge",
        "c3.8xlarge",
        "r3.large",
        "r3.xlarge",
        "r3.2xlarge",
        "r3.4xlarge",
        "r3.8xlarge",
        "i2.xlarge",
        "i2.2xlarge",
        "i2.4xlarge",
        "i2.8xlarge",
        "d2.xlarge",
        "d2.2xlarge",
        "d2.4xlarge",
        "d2.8xlarge",
        "g2.2xlarge",
        "g2.8xlarge"
      ],
      "Default" : "t2.medium"
    },

    "InstanceTypeBamboo" : {
      "Type" : "String",
      "Description" : "Will be used for Bamboo nodes",
      "AllowedValues" : [
        "t2.micro",
        "t2.small",
        "t2.medium",
        "t2.large",
        "m4.large",
        "m4.xlarge",
        "m4.2xlarge",
        "m4.4xlarge",
        "m4.10xlarge"
      ],
      "Default" : "t2.small"
    },

    "InstanceQtyBamboo" : {
      "Type" : "Number",
      "Description" : "Number of Bamboo nodes for this cluster",
      "Default" : "1"
    },

    "InstanceQtyAgent" : {
      "Type" : "Number",
      "Description" : "Number of mesos-agent nodes for this cluster",
      "Default" : "1"
    },

    "ChronosRestore" : {
      "Type" : "String",
      "Description" : "Attempt to restore Chronos from backup?  (Requires valid backup at s3://MY_BUCKET-west-versioned-backups/chronos/chronos_jobs_<stack>.bz2)",
      "AllowedValues" : [ "Yes", "No" ],
      "Default" : "No"
    },

    "ConsulServer" : {
      "Type" : "String",
      "Description" : "URL for the Consul server/cluster (no \"http://\")",
      "Default" : "consul-west.my.domain"
    },

    "VersionConsulTemplate" : {
      "Type" : "String",
      "Description" : "Version of consul-template to install (see https://github.com/hashicorp/consul-template/releases)",
      "Default" : "0.18.1",
      "AllowedValues" : [ "0.16.0", "0.18.1" ]
    },

    "VersionMesos" : {
      "Type" : "String",
      "Description" : "Version of Mesos to install on this cluster",
      "Default" : "1.1.0-2.0.107.ubuntu1604",
      "AllowedValues" : [
        "1.0.1-2.0.94.ubuntu1604",
        "1.1.0-2.0.107.ubuntu1604"
      ]
    },

    "VersionChefClient" : {
      "Type" : "String",
      "Description" : "Version of the Chef client to install on all nodes",
      "Default" : "12.7.2",
      "AllowedValues" : [
        "12.7.2"
      ]
    },

    "VersionMarathon" : {
      "Type" : "String",
      "Description" : "Version of Marathon to install on this cluster",
      "Default" : "1.4.1-1.0.635.ubuntu1604",
      "AllowedValues" : [
        "1.3.3-1.0.529.ubuntu1604",
        "1.3.6-1.0.540.ubuntu1604",
        "1.4.1-1.0.635.ubuntu1604"
      ]
    },

    "Sensu" : {
      "Type" : "String",
      "Description" : "Sensu API host",
      "Default" : "status.my.domain:4567"
    }

  },

  "Outputs" : {
    "DNSWeights" : {
      "Value" : "https://console.aws.amazon.com/route53/home#resource-record-sets:MY_DNS_ZONE",
      "Description" : "By default, DNS entries added to the master list are set with weight=0.  This should make for a smooth startup of this stack, but will cause problems if you don't manually set them higher before deploying another stack."
    },
    "Marathon" : {
      "Description" : "Marathon admin access",
      "Value" : { "Fn::Join" : [ "", [ "http://admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] }
    },
    "Mesos" : {
      "Description" : "Mesos admin access",
      "Value" : { "Fn::Join" : [ "", [ "http://admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":5050" ] ] }
    },
    "ServiceTraffic" : {
      "Description" : "Bamboo traffic routing",
      "Value" : { "Fn::Join" : [ "", [ "http://bamboo-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] }
    },
    "BambooAdmin" : {
      "Description" : "Bamboo administrative access",
      "Value" : { "Fn::Join" : [ "", [ "http://bamboo-admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":8000" ] ] }
    },
    "BambooMetrics" : {
      "Description" : "Bamboo metrics",
      "Value" : { "Fn::Join" : [ "", [ "http://bamboo-admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":9000" ] ] }
    },
    "ConsulGUI" : {
      "Description" : "Manage environment variables for this cluster.  (As provided in input parameters.)",
      "Value" : { "Fn::Join" : [ "", [ "http://", { "Ref" : "ConsulServer" }, ":8500" ] ] }
    },
    "Chronos" : {
      "Description" : "Create and manage scheduled jobs for this cluster.  Do yourself a major favor and use the API, not the web UI.  https://mesos.github.io/chronos/docs/api.html",
      "Value" : { "Fn::Join" : [ "", [ "http://admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":4400" ] ] }
    },
    "RedisAddress" : {
      "Condition" : "IsPrivate",
      "Description" : "Redis instance for queue management",
      "Value" : { "Fn::Join" : [ "", [ "redis-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":", { "Fn::GetAtt" : [ "Redis", "Outputs.RedisPort" ] } ] ] }
    }
  },

  "Mappings" : {
    "RegionMap" : {
      "us-east-1" : {
        "az1" : "us-east-1a",
        "az2" : "us-east-1c",
        "az3" : "us-east-1d",
        "ami" : "{{east_ami}}",
        "vpc" : "vpc-da65fdb4",
        "PrivSubnet1" : "subnet-d765fdb9",
        "PrivSubnet2" : "subnet-b1a89599",
        "PrivSubnet3" : "subnet-ae1d85c0",
        "PubSubnet1" : "subnet-d365fdbd",
        "PubSubnet2" : "subnet-a9a89581",
        "PubSubnet3" : "subnet-851c84eb",
        "Nat1Eip" : "54.208.130.132/32",
        "Nat2Eip" : "54.85.17.41/32",
        "Nat3Eip" : "54.208.88.100/32",
        "Development" : "sg-07fbc37d",
        "QA" : "sg-85fbc3ff",
        "UAT" : "sg-fffcc485",
        "Production" : "sg-6dfdc517",
        "PrivHostedZone" : "mesos.east",
        "CfnDeleteSensuClients": "arn:aws:lambda:us-east-1:1234567891011:function:CfnDeleteSensuClients:PROD",
        "CfnSilenceSensuClients": "arn:aws:lambda:us-east-1:1234567891011:function:CfnSilenceSensuClients:PROD"
      },
      "us-west-2" : {
        "az1" : "us-west-2a",
        "az2" : "us-west-2b",
        "az3" : "us-west-2c",
        "ami" : "{{west_ami}}",
        "vpc" : "vpc-429be227",
        "PrivSubnet1" : "subnet-fe3f719b",
        "PrivSubnet2" : "subnet-5c05692b",
        "PrivSubnet3" : "subnet-001e9859",
        "PubSubnet1" : "subnet-f93f719c",
        "PubSubnet2" : "subnet-5b05692c",
        "PubSubnet3" : "subnet-031e985a",
        "Nat1Eip" : "52.27.117.31/32",
        "Nat2Eip" : "52.88.128.233/32",
        "Nat3Eip" : "52.88.180.139/32",
        "Development" : "sg-f83ea59c",
        "QA" : "sg-f33ea597",
        "UAT" : "sg-fe3ea59a",
        "Production" : "sg-fc3ea598",
        "PrivHostedZone" : "mesos.west",
        "CfnDeleteSensuClients": "arn:aws:lambda:us-west-2:1234567891011:function:CfnDeleteSensuClients:PROD",
        "CfnSilenceSensuClients": "arn:aws:lambda:us-west-2:1234567891011:function:CfnSilenceSensuClients:PROD"
      }
    },
    "DomainOpts" : {
      "my.domain" : {
        "zone" : "MY_DNS_ZONE"
      }
    },
    "NumMasters" : {
      "Single" : {
        "quorum" : "1"
        },
      "Multi" : {
        "quorum" : "2"
      }
    },
    "env" : {
      "type" : {
        "Development" : "dev",
        "QA" : "qa",
        "UAT" : "uat",
        "Production" : "prod"
      },
      "access" : {
        "public" : "pub",
        "private" : "priv"
      }
    }

  },

  "Conditions" : {
    "IsProd" : { "Fn::Equals" : [ { "Ref" : "Environment" }, "prod" ] },
    "IsPublic" : { "Fn::Equals" : [ { "Ref" : "AccessMode" }, "public" ] },
    "IsPrivate" : { "Fn::Equals" : [ { "Ref" : "AccessMode" }, "private" ] },
    "MultiMasterCluster" : { "Fn::Equals" : [ { "Ref" : "InstanceQtyMaster" }, "Multi" ] },
    "RestoreChronos" : { "Fn::Equals" : [ { "Ref" : "ChronosRestore" }, "Yes" ] }
  },


  "Resources" : {

    "MesosIAMRole" : {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version" : "2012-10-17",
          "Statement": [ {
            "Action": [ "sts:AssumeRole" ],
            "Effect": "Allow",
            "Principal": { "Service": [ "ec2.amazonaws.com" ] }
          } ]
        },
        "Path": "/",
        "Policies" : [
          {
            "PolicyName" : { "Ref" : "AWS::StackName" },
            "PolicyDocument" : {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Action": [ "ec2:DescribeSecurityGroups" ],
                  "Effect": "Allow",
                  "Resource": [
                    { "Fn::Join" : [ "", [ "arn:aws:ec2:", { "Ref" : "AWS::Region"}, ":", { "Ref" : "AWS::AccountId" }, ":security-group/", { "Ref": "MesosAdminSecurityGroup" } ] ] },
                    { "Fn::Join" : [ "", [ "arn:aws:ec2:", { "Ref" : "AWS::Region"}, ":", { "Ref" : "AWS::AccountId" }, ":security-group/", { "Ref": "MesosPublicSecurityGroup" } ] ] },
                    { "Fn::Join" : [ "", [ "arn:aws:ec2:", { "Ref" : "AWS::Region"}, ":", { "Ref" : "AWS::AccountId" }, ":security-group/", { "Ref": "MesosPrivateSecurityGroup" } ] ] }
                  ]
                },
                {
                  "Action": [ "s3:GetObject" ],
                  "Effect": "Allow",
                  "Resource": [
                    "arn:aws:s3:::MY_BUCKET-docker/docker.tar.gz",
                    "arn:aws:s3:::MY_BUCKET-docker/docker_cleanup",
                    "arn:aws:s3:::MY_BUCKET-artifacts/*",
                    "arn:aws:s3:::MY_BUCKET-west-versioned-backups/consul/*",
                    "arn:aws:s3:::MY_BUCKET-west-versioned-backups/chronos/*"
                  ]
                },
                {
                  "Action": [ "s3:ListBucket" ],
                  "Effect": "Allow",
                  "Resource": [
                    "arn:aws:s3:::MY_BUCKET-artifacts"
                  ]
                },
                {
                  "Action": [ "s3:GetObject", "s3:PutObject" ],
                  "Effect": "Allow",
                  "Resource": [
                    "arn:aws:s3:::MY_BUCKET-east-versioned-backups/consul/*",
                    "arn:aws:s3:::MY_BUCKET-east-versioned-backups/chronos/*",
                    "arn:aws:s3:::MY_BUCKET-logs-dumps/mesos/*"
                  ]
                },
                {
                  "Action": [ "ec2:DescribeInstances" ],
                  "Effect": "Allow",
                  "Resource": [ "*" ]
                },
                {
                  "Action": [ "cloudformation:DescribeStacks" ],
                  "Effect": "Allow",
                  "Resource": [ "*" ]
                },
                {
                  "Action" : [ "route53:ChangeResourceRecordSets" ],
                  "Effect" : "Allow",
                  "Resource" : [
                    { "Fn::Join" : [ "", [ "arn:aws:route53:::hostedzone/", { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] } ] ] }
                  ]
                }
              ]
            }
          }
        ]
      }
    },

    "MesosIAMInstanceProfile" : {
      "Type": "AWS::IAM::InstanceProfile",
      "Properties": {
        "Path": "/",
        "Roles": [ { "Ref" : "MesosIAMRole" } ]
      }
    },

    "MesosAdminSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Security group for administering the Mesos cluster",
        "SecurityGroupIngress" : [
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : { "Ref" : "AdminIP" } },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : "10.66.0.0/16" }
        ],
        "VpcId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "vpc" ] },
        "Tags" : [ { "Key" : "Name", "Value" : { "Fn::Join" : [ "-", [ "admin", { "Ref" : "AWS::StackName" } ] ] } } ]
      }
    },

    "MesosPublicSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Security group for public access to the Mesos cluster",
        "SecurityGroupIngress" : [
          { "IpProtocol" : "TCP", "FromPort" : 80, "ToPort" : 80, "CidrIp" : "0.0.0.0/0" },
          { "IpProtocol" : "TCP", "FromPort" : 443, "ToPort" : 443, "CidrIp" : "0.0.0.0/0" }
        ],
        "VpcId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "vpc" ] },
        "Tags" : [ { "Key" : "Name", "Value" : { "Fn::Join" : [ "-", [ "public", { "Ref" : "AWS::StackName" } ] ] } } ]
      }
    },

    "MesosPrivateSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Metadata" : { "#FIXME" : "REMOVE THE ADMIN IP WHEN AGENT AUTOSCALING IS CONFIGURED" },
      "Properties" : {
        "GroupDescription" : "Security group for intra-cluster communication",
        "SecurityGroupIngress" : [
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : { "Ref" : "AdminIP" } },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "SourceSecurityGroupId" : { "Ref" : "MesosAdminSecurityGroup" } },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "SourceSecurityGroupId" : { "Ref" : "MesosPublicSecurityGroup" } },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : "10.66.0.0/16" },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "Nat1Eip" ] } },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "Nat2Eip" ] } },
          { "IpProtocol" : "TCP", "FromPort" : 0, "ToPort" : 65535, "CidrIp" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "Nat3Eip" ] } }
        ],
        "VpcId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "vpc" ] },
        "Tags" : [ { "Key" : "Name", "Value" : { "Fn::Join" : [ "-", [ "private", { "Ref" : "AWS::StackName" } ] ] } } ]
      }
    },

    "MesosPrivateSecurityGroupSelfIngress": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": { "Ref": "MesosPrivateSecurityGroup" },
        "IpProtocol": -1,
        "SourceSecurityGroupId": { "Ref": "MesosPrivateSecurityGroup" }
      }
    },

    "MesosAdminSecurityGroupSelfIngress": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": { "Ref": "MesosAdminSecurityGroup" },
        "IpProtocol": -1,
        "SourceSecurityGroupId": { "Ref": "MesosAdminSecurityGroup" }
      }
    },

    "DeleteSensuClients" : {
      "Type": "AWS::CloudFormation::CustomResource",
      "Metadata": {
        "Comment": "MasterInstance1 depends on this, so it will delete after all the server instances delete"
      },
      "Properties": {
        "ServiceToken": { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "CfnDeleteSensuClients" ] },
        "Region": { "Ref": "AWS::Region" },
        "StackName": { "Ref" : "AWS::StackName" },
        "Sensu": {"Ref": "Sensu"}
      }
    },

    "MasterInstance1" : {
      "Type" : "AWS::EC2::Instance",
      "DependsOn": "DeleteSensuClients",
      "CreationPolicy" : {
        "ResourceSignal" : {
          "Timeout" : "PT30M"
        }
      },
      "Metadata" : {
        "AWS::CloudFormation::Init" : {
          "configSets" : {
            "master" : [ "global_config", "fs_config", "java_config", "docker_config", "master_config", "chef_config" ],
            "agent" :  [ "global_config", "fs_config", "java_config", "docker_config", "agent_config", "chef_config" ],
            "bamboo" : [ "global_config", "fs_config", "docker_config", "bamboo_config", "chef_config" ]
          },
          "global_config" : {
            "files" : {
              "/root/cfn-init-scripts/repos.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "apt-key adv --keyserver keyserver.ubuntu.com --recv E56151BF\n",
                  "DISTRO=$(lsb_release -is | tr '[:upper:]' '[:lower:]')\n",
                  "CODENAME=$(lsb_release -cs)\n",
                  "echo \"deb http://repos.mesosphere.io/${DISTRO} ${CODENAME} main\" | tee /etc/apt/sources.list.d/mesosphere.list\n",
                  "add-apt-repository -y ppa:webupd8team/java\n",
                  "apt update\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/signal_on_first_boot.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash -eux\n",
                  "echo \"@reboot root while ! $SIGNAL; do sleep 1; done && rm /etc/cron.d/signal_cloudformation\" > /etc/cron.d/signal_cloudformation\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/silence_sensu.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash -eux\n",
                  "SENSU_URL=http://", { "Ref": "Sensu" }, "/stashes\n",
                  "SENSU_CLIENT_NAME=\"$(hostname)-$(curl -s 169.254.169.254/latest/meta-data/instance-id)\"\n",
                  "PAYLOAD_FILE=/root/cfn-init-scripts/silence-sensu-payload\n",
                  "PAYLOAD=\"{\\\"path\\\": \\\"silence/$SENSU_CLIENT_NAME\\\", \\\"expire\\\": 1200, \\\"content\\\": { \\\"reason\\\": \\\"CloudFormation stack launching\\\" }}\"\n",
                  "echo $PAYLOAD > $PAYLOAD_FILE\n",
                  "curl -s -i -X POST -H \"Content-Type: application/json\" -d @$PAYLOAD_FILE $SENSU_URL\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "00_fix_apt": {
                "command": "export DEBIAN_FRONTEND=\"noninteractive\""
              },
              "01_configure_repos": {
                "command": "/root/cfn-init-scripts/repos.sh"
              },
              "02_install_aws_tools": {
                "command": "apt install -y python-pip && pip install awscli"
              },
              "03_configure_signal_on_first_boot": {
                "command": "/root/cfn-init-scripts/signal_on_first_boot.sh"
              },
              "04_silence_sensu": {
                "command": "/root/cfn-init-scripts/silence_sensu.sh"
              }
            }
          },
          "chef_config" : {
            "files" : {
              "/root/cfn-init-scripts/install_chef.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "if hostname | grep bamboo\n",
                  "  then ROLE=bamboo\n",
                  "elif hostname | grep agent\n",
                  "  then ROLE=slave\n",
                  "elif hostname | grep master\n",
                  "  then ROLE=master\n",
                  "fi\n",
                  "wget -q https://opscode-omnibus-packages.s3.amazonaws.com/ubuntu/10.04/x86_64/chef_", { "Ref" : "VersionChefClient" }, "-1_amd64.deb\n",
                  "dpkg -i chef_", { "Ref" : "VersionChefClient" }, "-1_amd64.deb\n",
                  "mkdir -p /etc/chef/trusted_certs\n",
                  "aws --region us-east-1 s3 cp s3://MY_BUCKET-artifacts/chef/init.d/chef-auto /etc/init.d/chef-auto\n",
                  "aws --region us-east-1 s3 cp s3://MY_BUCKET-artifacts/chef/default/chef-auto /etc/default/chef-auto\n",
                  "aws --region us-east-1 s3 cp s3://MY_BUCKET-artifacts/chef/validation.pem /etc/chef/validation.pem\n",
                  "aws --region us-east-1 s3 cp s3://MY_BUCKET-artifacts/chef/trusted_certs/prod-vpc-mesos-chef.crt /etc/chef/trusted_certs/prod-vpc-mesos-chef.crt\n",
                  "echo NODE_NAME=\"$(hostname)-\\${INSTANCE_ID}\" >> /etc/default/chef-auto\n",
                  "echo NODE_ENV=\"", { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "\" >> /etc/default/chef-auto\n",
                  "sed -i \"s/role\\[base\\]/role\\[base\\],role\\[$ROLE\\]/\" /etc/default/chef-auto\n",
                  "chmod +x /etc/init.d/chef-auto\n",
                  "chmod 600 /etc/chef/validation.pem\n",
                  "update-rc.d chef-auto defaults 80 20\n",
                  "/etc/init.d/chef-auto start\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "01_install_chef": {
                "command": "/root/cfn-init-scripts/install_chef.sh"
              }
            }
          },
          "master_config" : {
            "files" : {
              "/etc/cron.daily/restart_chronos" : {
                "content" : "#!/bin/bash\nsystemctl restart chronos",
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/etc/systemd/system/zookeeper.service" : {
                "content" : { "Fn::Join" : ["", [
                  "[Unit]\n",
                  "Description=Zookeeper service\n",
                  "After=docker.service\n",
                  "Requires=docker.service\n\n",
                  "[Install]\n",
                  "WantedBy=multi-user.target\n\n",
                  "[Service]\n",
                  "TimeoutStartSec=0\n",
                  "Restart=always\n",
                  "ExecStop=-/usr/bin/docker stop %n\n",
                  "ExecStopPost=-/usr/bin/docker rm %n\n",
                  "ExecStartPre=-/usr/bin/docker stop %n\n",
                  "ExecStartPre=-/usr/bin/docker rm %n\n",
                  "ExecStartPre=/usr/bin/docker pull zookeeper:latest\n"
                ]]},
                "mode"  : "000644",
                "owner" : "root",
                "group" : "root"
              },
              "/etc/zoo.cfg": {
                "content" : { "Fn::Join" : [ "", [
                  "clientPort=2181\n",
                  "dataDir=/data\n",
                  "tickTime=2000\n",
                  "initLimit=30\n",
                  "syncLimit=30\n",
                  "cnxTimeout=300\n",
                  { "Fn::If" : [ "MultiMasterCluster",
                    { "Fn::Join" : [ "", [
                      "server.1=vpc-", { "Ref" : "AWS::StackName" }, "-master1.", { "Ref" : "Domain" }, ":2888:3888\n",
                      "server.2=vpc-", { "Ref" : "AWS::StackName" }, "-master2.", { "Ref" : "Domain" }, ":2888:3888\n",
                      "server.3=vpc-", { "Ref" : "AWS::StackName" }, "-master3.", { "Ref" : "Domain" }, ":2888:3888\n"
                    ] ] }, "" ]
                  }

                ]]},
                "mode"  : "000644",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/install_zookeeper.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "ZOO_ID=$(echo -n $(hostname) | tail -c -1)\n",
                  "COMMAND=\"/usr/bin/docker run ",
                    "--net host ",
                    "--name %n ",
                    "-e ZOO_MY_ID=$ZOO_ID ",
                    "-v /var/lib/zookeeper:/data ",
                    "-v /etc/zoo.cfg:/conf/zoo.cfg ",
                    "zookeeper:latest\"\n",
                  "echo \"ExecStart=$COMMAND\" >> /etc/systemd/system/zookeeper.service\n",
                  "COMMAND=",
                  { "Fn::If" : [ "MultiMasterCluster",
                    "\"/bin/bash -c \\\"until ( /usr/bin/docker logs %n 2>&1 | /bin/grep -m1 \\\\\\\"LEADER ELECTION TOOK\\\\\\\"); do /bin/sleep 1; done\\\"\"\n",
                    "\"/bin/bash -c \\\"until ( /usr/bin/docker logs %n 2>&1 | /bin/grep -m1 \\\\\\\"binding to port\\\\\\\"); do /bin/sleep 1; done\\\"\"\n"
                  ]},
                  "echo \"ExecStartPost=$COMMAND\" >> /etc/systemd/system/zookeeper.service\n",
                  "systemctl enable zookeeper\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/etc/systemd/system/mesos-master.service.d/zookeeperdependency.conf" : {
                "content" : "[Unit]\nAfter=zookeeper.service\nRequires=zookeeper.service\n",
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/install_mesos_marathon.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "apt install -y mesos=", { "Ref" : "VersionMesos" }, " ",
                    "marathon=", { "Ref" : "VersionMarathon" }, " ",
                    "python-pip jq zookeeper-\n",
                  "pip install awscli\n",
                  "systemctl stop marathon; systemctl stop mesos-master\n",
                  "systemctl enable mesos-master marathon\n",
                  "echo zk://",
                    "vpc-", { "Ref" : "AWS::StackName" }, "-master1.", { "Ref" : "Domain" }, ":2181",
                    { "Fn::If" : [ "MultiMasterCluster",
                      { "Fn::Join" : [ "", [
                        ",vpc-", { "Ref" : "AWS::StackName" }, "-master2.", { "Ref" : "Domain" }, ":2181",
                        ",vpc-", { "Ref" : "AWS::StackName" }, "-master3.", { "Ref" : "Domain" }, ":2181"
                      ] ] }, "" ]
                    },
                    "/mesos > /etc/mesos/zk\n",
                  "echo ", { "Ref" : "AWS::StackName" }, " | tee /etc/mesos/cluster /etc/mesos-master/cluster\n",
                  "echo ", { "Fn::FindInMap" : [ "NumMasters", { "Ref" : "InstanceQtyMaster" }, "quorum" ] }, " > /etc/mesos-master/quorum\n",
                  "echo true > /etc/mesos-master/log_auto_initialize\n",
                  "mkdir -p /etc/marathon/conf\n",
                  "echo role_", { "Ref" : "AccessMode" }, " | tee /etc/mesos-master/roles /etc/marathon/conf/default_accepted_resource_roles /etc/marathon/conf/mesos_role\n",
                  "echo http_callback > /etc/marathon/conf/event_subscriber\n",
                  "echo $(hostname).", { "Ref": "Domain" }, " > /etc/mesos-master/hostname\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/etc/systemd/system/chronos.service" : {
                "content" : { "Fn::Join" : ["", [
                  "[Unit]\n",
                  "Description=Chronos service\n",
                  "After=network.target\n",
                  "After=mesos-master.service\n",
                  "Requires=network.target\n",
                  "Requires=zookeeper.service\n",
                  "Requires=mesos-master.service\n\n",
                  "[Install]\n",
                  "WantedBy=multi-user.target\n\n",
                  "[Service]\n",
                  "TimeoutStartSec=60\n",
                  "Restart=always\n",
                  "RestartSec=10\n",
                  "ExecStop=/usr/bin/docker stop %n\n",
                  "ExecStopPost=/usr/bin/docker rm %n\n",
                  "ExecStartPre=-/usr/bin/docker stop %n\n",
                  "ExecStartPre=-/usr/bin/docker rm %n\n",
                  "ExecStartPre=sleep 30 # Wait for Zookeeper to start\n",
                  "ExecStartPre=/usr/bin/docker pull ", { "Ref" : "ChronosImage" }, "\n",
                  "ExecStartPost=/bin/bash -c \"until ( /usr/bin/docker logs %n 2>&1 | /bin/grep -m1 \\\"All services up and running.\\\"); do /bin/sleep 1; done\"\n"
                ]]},
                "mode"  : "000644",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/install_chronos.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "COMMAND=\"/usr/bin/docker run ",
                    "--net=host ",
                    "-e PORT0=4400 ",
                    "-e PORT1=8081 ",
                    "--name %n ",
                    { "Ref" : "ChronosImage" }, " ",
                    "--zk_hosts=$(awk -F/ '{print $3}' /etc/mesos/zk) ",
                    "--master=$(cat /etc/mesos/zk) ",
                    "--hostname=$(hostname) ",
                    "--mesos_role=$(cat /etc/mesos-master/roles) ",
                    "--mesos_framework_name=chronos\"\n",
                  "echo \"ExecStart=$COMMAND\" >> /etc/systemd/system/chronos.service\n",
                  "systemctl enable chronos\n",
                  "echo \"@reboot root /root/cfn-init-scripts/install_chronos_backup_job.sh && rm /etc/cron.d/chronos_backup_job\" >> /etc/cron.d/chronos_backup_job\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/chronos_backup_job.json": {
                "content": { "Fn::Join" : ["", [
                  "{ \"name\": \"Chronos Backup\",\n",
                  "  \"command\": \"docker --config /etc/docker/.docker pull MY_DOCKER_ORG/docker-utility; ",
                                  "docker --config /etc/docker/.docker run --rm --env-file /etc/consul-template/docker MY_DOCKER_ORG/docker-utility /s3_run.sh chronos/chronos_backup.sh ", { "Ref" : "AWS::StackName" },"\",\n",
                  "  \"schedule\": \"R//PT15M\", \n",
                  "  \"scheduleTimeZone\": \"\", \n",
                  "  \"epsilon\": \"PT30M\", \n",
                  "  \"owner\": \"admins@sample.com\", \n",
                  "  \"ownerName\": \"Admins\", \n",
                  "  \"description\": \"Backup the Chronos jobs\", \n",
                  "  \"async\": false\n",
                  "}\n"
                ]]},
                "mode"  : "000664",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/install_chronos_backup_job.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "# Create a job to back up this Chronos cluster's jobs\n",
                  "until curl -L -H 'Content-Type: application/json' -X POST -d @/root/cfn-init-scripts/chronos_backup_job.json http://admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":4400/scheduler/iso8601\n",
                  "  do : \n",
                  "done\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "10_install_zookeeper": {
                "command": "/root/cfn-init-scripts/install_zookeeper.sh"
              },
              "20_install_mesos_marathon": {
                "command": "/root/cfn-init-scripts/install_mesos_marathon.sh"
              },
              "30_install_chronos": {
                "command": "/root/cfn-init-scripts/install_chronos.sh"
              }
            }
          },
          "java_config" : {
            "files" : {
              "/root/cfn-init-scripts/install_java.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "apt install -y debconf-utils\n",
                  "echo oracle-java8-installer  shared/accepted-oracle-license-v1-1 boolean true | debconf-set-selections\n",
                  "apt install --force-yes -y oracle-java8-installer oracle-java8-set-default\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "01_install_java": {
                "command": "/root/cfn-init-scripts/install_java.sh"
              }
            }
          },
          "fs_config" : {
            "commands": {
              "01_mkfs": {
                "command": "mkfs.ext4 -N 2000000 /dev/xvdb"
              },
              "02_fstab": {
                "command": "echo /dev/xvdb /var/lib/docker ext4 defaults,discard 0 0 >> /etc/fstab"
              },
              "03_mount": {
                "command": "mkdir -p /var/lib/docker && mount /var/lib/docker"
              }
            }
          },
          "docker_config" : {
            "files" : {
              "/root/cfn-init-scripts/install_docker.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "apt update\n",
                  "apt install -y docker.io\n",
                  "#while [ ! -x /usr/bin/docker ]; do curl -sSL https://get.docker.com/ | sh; done\n",
                  "aws --region us-west-2 s3 cp s3://MY_BUCKET-docker/docker.tar.gz /etc/docker.tar.gz\n",
                  "tar xvfz /etc/docker.tar.gz --directory /etc/docker/\n",
                  "echo '{\"storage-driver\":\"overlay2\"}' > /etc/docker/daemon.json\n",
                  "aws --region us-west-2 s3 cp s3://MY_BUCKET-docker/docker_cleanup /etc/cron.hourly/ && chmod +x /etc/cron.hourly/docker_cleanup\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "01_install_docker": {
                "command": "/root/cfn-init-scripts/install_docker.sh"
              }
            }
          },
          "agent_config" : {
            "files" : {
              "/root/cfn-init-scripts/install_mesos.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "echo manual | tee /etc/init/zookeeper.override /etc/init/mesos-master.override\n",
                  "apt -y update && apt install -y unzip mesos=", { "Ref" : "VersionMesos" }, "\n",
                  "systemctl stop mesos-slave; systemctl stop mesos-master\n",
                  "#echo $(hostname).", { "Ref": "Domain" }, " > /etc/mesos-slave/hostname\n",
                  "curl http://169.254.169.254/latest/meta-data/local-ipv4 > /etc/mesos-slave/advertise_ip\n",
                  "echo zk://",
                    "vpc-", { "Ref" : "AWS::StackName" }, "-master1.", { "Ref" : "Domain" }, ":2181",
                    { "Fn::If" : [ "MultiMasterCluster",
                      { "Fn::Join" : [ "", [
                        ",vpc-", { "Ref" : "AWS::StackName" }, "-master2.", { "Ref" : "Domain" }, ":2181",
                        ",vpc-", { "Ref" : "AWS::StackName" }, "-master3.", { "Ref" : "Domain" }, ":2181"
                      ] ] }, ""
                    ] },
                    "/mesos > /etc/mesos/zk\n",
                  "echo docker,mesos > /etc/mesos-slave/containerizers\n",
                  "echo role_", { "Ref" : "AccessMode" }, " > /etc/mesos-slave/default_role\n",
                  "echo 5mins > /etc/mesos-slave/executor_registration_timeout\n",
                  "echo false > /etc/mesos-slave/hostname_lookup\n",
                  "systemctl enable mesos-slave\n",
                  "systemctl start mesos-slave\n",
                  "mkdir /var/log/apps && chown root:daemon /var/log/apps && chmod 775 /var/log/apps\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/etc/systemd/system/consul-template.service" : {
                "content" : { "Fn::Join" : ["", [
                  "[Unit]\n",
                  "Description=Consul template Docker environment variables\n\n",
                  "[Service]\n",
                  "ExecStart=/usr/local/bin/consul-template -consul ", { "Ref" : "ConsulServer" }, ":8500 -template \"/etc/consul-template/docker.ctmpl:/etc/consul-template/docker:\"\n\n",
                  "[Install]\n",
                  "WantedBy=multi-user.target mesos-slave.service\n"
                ]]},
                "mode"  : "000644",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/install_consul_template.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "wget https://releases.hashicorp.com/consul-template/", { "Ref" : "VersionConsulTemplate" }, "/consul-template_", { "Ref" : "VersionConsulTemplate" }, "_linux_amd64.zip\n",
                  "unzip consul-template_", { "Ref" : "VersionConsulTemplate" }, "_linux_amd64.zip -d /usr/local/bin/ && chmod 755 /usr/local/bin/consul-template\n",
                  "mkdir /etc/consul-template\n",
                  "aws --region us-east-1 s3 cp s3://MY_BUCKET-artifacts/consul-template/docker.ctmpl /etc/consul-template\n",
                  "aws --region us-east-1 s3 cp s3://MY_BUCKET-artifacts/consul-template/restart_apps.sh /etc/consul-template\n",
                  "sleep 2\n",
                  "sed -i -e 's/PREFIX/", { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "/' /etc/consul-template/docker.ctmpl\n",
                  "consul-template -once -consul ", { "Ref" : "ConsulServer" }, ":8500 -template \"/etc/consul-template/docker.ctmpl:/etc/consul-template/docker:\"\n",
                  "systemctl enable consul-template\n",
                  "systemctl start consul-template\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/chronos_backup_job.json": {
                "content": { "Fn::Join" : ["", [
                  "{ \"name\": \"Chronos Backup\",\n",
                  "  \"command\": \"docker --config /etc/docker/.docker pull MY_DOCKER_ORG/docker-utility; ",
                                  "docker --config /etc/docker/.docker run --rm --env-file /etc/consul-template/docker MY_DOCKER_ORG/docker-utility /s3_run.sh chronos/chronos_backup.sh ", { "Ref" : "AWS::StackName" },"\",\n",
                  "  \"schedule\": \"R//PT15M\", \n",
                  "  \"scheduleTimeZone\": \"\", \n",
                  "  \"epsilon\": \"PT30M\", \n",
                  "  \"owner\": \"admins@sample.com\", \n",
                  "  \"ownerName\": \"Admins\", \n",
                  "  \"description\": \"Backup the Chronos jobs\", \n",
                  "  \"async\": false\n",
                  "}\n"
                ]]},
                "mode"  : "000664",
                "owner" : "root",
                "group" : "root"
              },
              "/root/cfn-init-scripts/install_chronos_backup_job.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "# Create a job to back up this Chronos cluster's jobs\n",
                  "until curl -L -H 'Content-Type: application/json' -X POST -d @/root/cfn-init-scripts/chronos_backup_job.json http://admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" }, ":4400/scheduler/iso8601\n",
                  "  do : \n",
                  "done\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "01_install_mesos": {
                "command": "/root/cfn-init-scripts/install_mesos.sh"
              },
              "02_install_consul_template": {
                "command": "/root/cfn-init-scripts/install_consul_template.sh"
              },
              "04_install_chronos_backup_job": {
                "command": "#/root/cfn-init-scripts/install_chronos_backup_job.sh"
              }
            }
          },
          "bamboo_config" : {
            "files" : {
              "/root/cfn-init-scripts/install_bamboo.sh" : {
                "content" : { "Fn::Join" : ["", [
                  "#!/bin/bash\n",
                  "COMMAND=\"docker --config='/etc/docker/.docker' run -d -p 8000:8000 -p 9000:9000 -p 80:80 -p 443:443 ",
                    "-e MARATHON_ENDPOINT=",
                    "http://vpc-", { "Ref" : "AWS::StackName" }, "-master1.", { "Ref" : "Domain" }, ":8080",
                    { "Fn::If" : [ "MultiMasterCluster",
                      { "Fn::Join" : [ "", [
                        ",http://vpc-", { "Ref" : "AWS::StackName" }, "-master2.", { "Ref" : "Domain" }, ":8080",
                        ",http://vpc-", { "Ref" : "AWS::StackName" }, "-master3.", { "Ref" : "Domain" }, ":8080"
                      ] ] },
                      "" ]
                    },
                    " -e BAMBOO_ENDPOINT=http://",
                    "$(curl --silent http://169.254.169.254/latest/meta-data/local-ipv4)",
                    ":8000 ",
                    "-e BAMBOO_ZK_HOST=",
                    "vpc-", { "Ref" : "AWS::StackName" }, "-master1.", { "Ref" : "Domain" }, ":2181",
                    { "Fn::If" : [ "MultiMasterCluster",
                      { "Fn::Join" : [ "", [
                        ",vpc-", { "Ref" : "AWS::StackName" }, "-master2.", { "Ref" : "Domain" }, ":2181",
                        ",vpc-", { "Ref" : "AWS::StackName" }, "-master3.", { "Ref" : "Domain" }, ":2181"
                      ] ] },
                      "" ]
                    },
                    " -e BAMBOO_ZK_PATH=/bamboo ",
                    "-e BIND=\":8000\" ",
                    "-e CONFIG_PATH=\"config/production.example.json\" ",
                    "-e BAMBOO_DOCKER_AUTO_HOST=true ",
                    "MY_DOCKER_ORG/qubit-bamboo:47-79021c2-0.2.21\"\n",

                  "echo \"@reboot root $COMMAND\" > /etc/cron.d/start_bamboo\n",
                  "echo $COMMAND\n",
                  "$COMMAND\n"
                ]]},
                "mode"  : "000775",
                "owner" : "root",
                "group" : "root"
              }
            },
            "commands": {
              "01_install_bamboo": {
                "command": "/root/cfn-init-scripts/install_bamboo.sh"
              }
            }
          }

        }
      },
      "Properties" : {
        "BlockDeviceMappings" : [
           { "DeviceName" : "/dev/sda1", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true" } },
           { "DeviceName" : "/dev/sdb", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true"} }
        ],
        "IamInstanceProfile" : { "Ref" : "MesosIAMInstanceProfile" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "ami" ] },
        "InstanceType" : { "Ref" : "InstanceTypeMaster" },
        "KeyName" : { "Ref" : "SshKey" },
        "Tags" : [
          { "Key" : "Name", "Value" : { "Fn::Join" : [ "-", [ "vpc", { "Ref" : "AWS::StackName" }, "master1" ] ] } },
          { "Key" : "ZONE", "Value" : { "Fn::Join" : [ "", [ { "Ref" : "Domain" }, "." ] ] } },
          { "Key" : "CNAME", "Value" : { "Fn::Join" : [ "", [ "vpc-", { "Ref" : "AWS::StackName" }, "-master1.", { "Ref" : "Domain" }, "." ] ] } }
        ],
        "Tenancy" : "default",
        "NetworkInterfaces" : [
          {
            "GroupSet" : [
              { "Ref" : "MesosPrivateSecurityGroup" },
              { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
            ],
            "AssociatePublicIpAddress" : false,
            "DeleteOnTermination" : true,
            "DeviceIndex" : 0,
            "SubnetId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet1" ] }
          }
        ],
        "UserData" : { "Fn::Base64" :
          { "Fn::Join" : [ "", [
            "#!/bin/bash -vux\n",
            "NAME=", "vpc-", { "Ref" : "AWS::StackName" }, "-master1\n",
            "echo $(curl http://169.254.169.254/latest/meta-data/local-ipv4) $NAME $NAME.", { "Ref": "Domain" }, ">> /etc/hosts\n",
            "hostnamectl set-hostname $NAME\n",
            "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
            "apt update\n",
            "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
            "apt install -y python-setuptools \n",
            "mkdir aws-cfn-bootstrap-latest\n",
            "curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1 \n",
            "easy_install aws-cfn-bootstrap-latest \n",
            "SIGNAL=\"/usr/local/bin/cfn-signal --exit-code 0 --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --region ", { "Ref" : "AWS::Region" }, "\"\n",
            "export SIGNAL\n",
            "if /usr/local/bin/cfn-init --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --configsets \"master\" --region ", { "Ref" : "AWS::Region" }, "\n",
              "then reboot\n",
            "else\n",
              "echo Seems that something went wrong with the cfn-init scripts... See /var/log/cfn-init-cmd.log for more info.\n",
              "/usr/local/bin/cfn-signal --exit-code 1 --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --region ", { "Ref" : "AWS::Region" }, "\n",
            "fi\n"
          ] ] }
        }
      }
    },

    "MasterInstance2" : {
      "Type" : "AWS::EC2::Instance",
      "Condition" : "MultiMasterCluster",
      "DependsOn": "DeleteSensuClients",
      "CreationPolicy" : {
        "ResourceSignal" : {
          "Timeout" : "PT30M"
        }
      },
      "Properties" : {
        "BlockDeviceMappings" : [
           { "DeviceName" : "/dev/sda1", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true" } },
           { "DeviceName" : "/dev/sdb", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true"} }
        ],
        "IamInstanceProfile" : { "Ref" : "MesosIAMInstanceProfile" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "ami" ] },
        "InstanceType" : { "Ref" : "InstanceTypeMaster" },
        "KeyName" : { "Ref" : "SshKey" },
        "Tags" : [
          { "Key" : "Name", "Value" : { "Fn::Join" : [ "-", [ "vpc", { "Ref" : "AWS::StackName" }, "master2" ] ] } },
          { "Key" : "ZONE", "Value" : { "Fn::Join" : [ "", [ { "Ref" : "Domain" }, "." ] ] } },
          { "Key" : "CNAME", "Value" : { "Fn::Join" : [ "", [ "vpc-", { "Ref" : "AWS::StackName" }, "-master2.", { "Ref" : "Domain" }, "." ] ] } }
        ],
        "Tenancy" : "default",
        "NetworkInterfaces" : [
          {
            "GroupSet" : [
              { "Ref" : "MesosPrivateSecurityGroup" },
              { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
            ],
            "AssociatePublicIpAddress" : false,
            "DeleteOnTermination" : true,
            "DeviceIndex" : 0,
            "SubnetId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet2" ] }
          }
        ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : [ "", [
          "#!/bin/bash -vux\n",
          "NAME=", "vpc-", { "Ref" : "AWS::StackName" }, "-master2\n",
          "echo $(curl http://169.254.169.254/latest/meta-data/local-ipv4) $NAME $NAME.", { "Ref": "Domain" }, ">> /etc/hosts\n",
          "hostnamectl set-hostname $NAME\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
          "apt update\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
          "apt install -y python-setuptools \n",
          "mkdir aws-cfn-bootstrap-latest\n",
          "curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1 \n",
          "easy_install aws-cfn-bootstrap-latest \n",
          "SIGNAL=\"/usr/local/bin/cfn-signal --exit-code 0 --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance2 --region ", { "Ref" : "AWS::Region" }, "\"\n",
          "export SIGNAL\n",
          "/usr/local/bin/cfn-init --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --configsets \"master\" --region ", { "Ref" : "AWS::Region" }, "\n",
          "reboot\n"
        ] ] } }
      }
    },

    "MasterInstance3" : {
      "Type" : "AWS::EC2::Instance",
      "Condition" : "MultiMasterCluster",
      "DependsOn": "DeleteSensuClients",
      "CreationPolicy" : {
        "ResourceSignal" : {
          "Timeout" : "PT30M"
        }
      },
      "Properties" : {
        "BlockDeviceMappings" : [
           { "DeviceName" : "/dev/sda1", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true" } },
           { "DeviceName" : "/dev/sdb", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true"} }
        ],
        "IamInstanceProfile" : { "Ref" : "MesosIAMInstanceProfile" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "ami" ] },
        "InstanceType" : { "Ref" : "InstanceTypeMaster" },
        "KeyName" : { "Ref" : "SshKey" },
        "Tags" : [
          { "Key" : "Name", "Value" : { "Fn::Join" : [ "-", [ "vpc", { "Ref" : "AWS::StackName" }, "master3" ] ] } },
          { "Key" : "ZONE", "Value" : { "Fn::Join" : [ "", [ { "Ref" : "Domain" }, "." ] ] } },
          { "Key" : "CNAME", "Value" : { "Fn::Join" : [ "", [ "vpc-", { "Ref" : "AWS::StackName" }, "-master3.", { "Ref" : "Domain" }, "." ] ] } }
        ],
        "Tenancy" : "default",
        "NetworkInterfaces" : [
          {
            "GroupSet" : [
              { "Ref" : "MesosPrivateSecurityGroup" },
              { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
            ],
            "AssociatePublicIpAddress" : false,
            "DeleteOnTermination" : true,
            "DeviceIndex" : 0,
            "SubnetId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet3" ] }
          }
        ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : [ "", [
          "#!/bin/bash -vux\n",
          "NAME=", "vpc-", { "Ref" : "AWS::StackName" }, "-master3\n",
          "echo $(curl http://169.254.169.254/latest/meta-data/local-ipv4) $NAME $NAME.", { "Ref": "Domain" }, ">> /etc/hosts\n",
          "hostnamectl set-hostname $NAME\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
          "apt update\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
          "apt install -y python-setuptools \n",
          "mkdir aws-cfn-bootstrap-latest\n",
          "curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1 \n",
          "easy_install aws-cfn-bootstrap-latest \n",
          "SIGNAL=\"/usr/local/bin/cfn-signal --exit-code 0 --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance3 --region ", { "Ref" : "AWS::Region" }, "\"\n",
          "export SIGNAL\n",
          "/usr/local/bin/cfn-init --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --configsets \"master\" --region ", { "Ref" : "AWS::Region" }, "\n",
          "reboot\n"
        ] ] } }
      }
    },

    "AdminLoadBalancer" : {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "DependsOn" : "MasterInstance1",
      "Properties": {
        "Scheme" : "internal",
        "Listeners" : [
          {
            "InstancePort" : "8080",
            "InstanceProtocol" : "HTTP",
            "LoadBalancerPort" : "80",
            "Protocol" : "HTTP"
          },
          {
            "InstancePort" : "5050",
            "InstanceProtocol" : "HTTP",
            "LoadBalancerPort" : "5050",
            "Protocol" : "HTTP"
          },
          {
            "InstancePort" : "2181",
            "InstanceProtocol" : "TCP",
            "LoadBalancerPort" : "2181",
            "Protocol" : "TCP"
          },
          {
            "InstancePort" : "4400",
            "InstanceProtocol" : "TCP",
            "LoadBalancerPort" : "4400",
            "Protocol" : "TCP"
          }
        ],
        "HealthCheck": {
          "HealthyThreshold" : "2",
          "Interval" : "15",
          "Target" : "HTTP:5050/",
          "Timeout" : "5",
          "UnhealthyThreshold" : "2"
        },
        "Instances" : { "Fn::If" : ["MultiMasterCluster",
          [ { "Ref" : "MasterInstance1" }, { "Ref" : "MasterInstance2" }, { "Ref" : "MasterInstance3" }  ],
          [ { "Ref" : "MasterInstance1" } ]
        ] },
        "SecurityGroups" : [
          { "Ref" : "MesosAdminSecurityGroup" },
          { "Ref" : "MesosPrivateSecurityGroup" },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
        ],
        "Subnets": [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet3" ] }
        ]
      }
    },

    "AdminLoadBalancerDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "AdminLoadBalancer",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::GetAtt" : [ "AdminLoadBalancer", "DNSName" ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60"
      }
    },

    "AgentLaunchConfig" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "AssociatePublicIpAddress" : false,
        "KeyName" : { "Ref" : "SshKey" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "ami" ] },
        "IamInstanceProfile" : { "Ref" : "MesosIAMInstanceProfile" },
        "SecurityGroups" : [
          { "Ref" : "MesosPrivateSecurityGroup" },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
        ],
        "InstanceType" : { "Ref" : "InstanceTypeAgent" },
        "BlockDeviceMappings" : [
           { "DeviceName" : "/dev/sda1", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true" } },
           { "DeviceName" : "/dev/sdb", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true"} }
        ],
        "UserData" : { "Fn::Base64" : { "Fn::Join" : [ "", [
          "#!/bin/bash -v\n",
          "INSTANCE_ID=$(curl --silent http://169.254.169.254/latest/meta-data/instance-id | tail -c8)\n",
          "NAME=", "vpc-", { "Ref" : "AWS::StackName" }, "-agent-$INSTANCE_ID\n",
          "echo $(curl http://169.254.169.254/latest/meta-data/local-ipv4) $NAME $NAME.", { "Ref": "Domain" }, ">> /etc/hosts\n",
          "hostnamectl set-hostname $NAME\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
          "apt update\n",
          "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
          "apt install -y python-setuptools \n",
          "mkdir aws-cfn-bootstrap-latest\n",
          "curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1 \n",
          "easy_install aws-cfn-bootstrap-latest \n",
          "SIGNAL=\"/usr/local/bin/cfn-signal --exit-code $? --stack ", { "Ref" : "AWS::StackName" }, " --resource AgentScalingGroup --region ", { "Ref" : "AWS::Region" }, "\"\n",
          "export SIGNAL\n",
          "/usr/local/bin/cfn-init --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --configsets \"agent\" --region ", { "Ref" : "AWS::Region" }, "\n",
          "reboot\n"
        ] ] } }
      }
    },

    "AgentScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "DependsOn" : "MasterInstance1",
      "CreationPolicy": {
        "ResourceSignal": {
          "Count": { "Ref" : "InstanceQtyAgent" },
          "Timeout": "PT30M"
        }
      },
      "Properties" : {
        "AvailabilityZones" : [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "az1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "az2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "az3" ] }
        ],
        "LaunchConfigurationName" : { "Ref" : "AgentLaunchConfig" },
        "MinSize" : { "Ref" : "InstanceQtyAgent" },
        "MaxSize" : { "Ref" : "InstanceQtyAgent" },
        "MetricsCollection": [ { "Granularity": "1Minute", "Metrics": [ "GroupMinSize", "GroupMaxSize" ] } ],
        "VPCZoneIdentifier" : [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet3" ] }
        ],
        "Tags" : [ {
          "Key" : "Name",
          "Value" : { "Fn::Join" : [ "-", [ { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "vpc", { "Ref" : "AWS::StackName" }, "agent" ] ] },
          "PropagateAtLaunch" : "true"
        } ]
      }
    },

    "BambooLaunchConfig" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "AssociatePublicIpAddress" : false,
        "KeyName" : { "Ref" : "SshKey" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "ami" ] },
        "IamInstanceProfile" : { "Ref" : "MesosIAMInstanceProfile" },
        "SecurityGroups" : [
          { "Ref" : "MesosPrivateSecurityGroup" },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
        ],
        "InstanceType" : { "Ref" : "InstanceTypeBamboo" },
        "BlockDeviceMappings" : [
           { "DeviceName" : "/dev/sda1", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true" } },
           { "DeviceName" : "/dev/sdb", "Ebs" : { "VolumeSize" : "16", "VolumeType" : "gp2", "DeleteOnTermination" : "true"} }
        ],
        "UserData" : { "Fn::Base64" :
          { "Fn::Join" : [ "", [
            "#!/bin/bash -vux\n",
            "NAME=", "vpc-", { "Ref" : "AWS::StackName" }, "-bamboo\n",
            "echo $(curl http://169.254.169.254/latest/meta-data/local-ipv4) $NAME $NAME.", { "Ref": "Domain" }, ">> /etc/hosts\n",
            "hostnamectl set-hostname $NAME\n",
            "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
            "apt update\n",
            "while lsof /var/lib/apt/lists/lock || lsof /var/cache/apt/archives/lock || lsof /var/lib/dpkg/lock; do echo waiting for other apt process to stop; sleep 30; done\n",
            "apt install -y python-setuptools \n",
            "mkdir aws-cfn-bootstrap-latest\n",
            "curl https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz | tar xz -C aws-cfn-bootstrap-latest --strip-components 1 \n",
            "easy_install aws-cfn-bootstrap-latest \n",
            "SIGNAL=\"/usr/local/bin/cfn-signal --exit-code $? --stack ", { "Ref" : "AWS::StackName" }, " --resource BambooScalingGroup --region ", { "Ref" : "AWS::Region" }, "\"\n",
            "export SIGNAL\n",
            "/usr/local/bin/cfn-init --stack ", { "Ref" : "AWS::StackName" }, " --resource MasterInstance1 --configsets \"bamboo\" --region ", { "Ref" : "AWS::Region" }, "\n",
            "reboot\n"
          ] ] }
        }
      }
    },

    "BambooScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "DependsOn" : [ "MasterInstance1" ],
      "CreationPolicy": {
        "ResourceSignal": {
          "Count": { "Ref": "InstanceQtyBamboo" },
          "Timeout": "PT20M"
        }
      },
      "Properties" : {
        "AvailabilityZones" : [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "az1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "az2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "az3" ] }
        ],
        "LaunchConfigurationName" : { "Ref" : "BambooLaunchConfig" },
        "MinSize" : { "Ref" : "InstanceQtyBamboo" },
        "MaxSize" : { "Ref" : "InstanceQtyBamboo" },
        "MetricsCollection": [ { "Granularity": "1Minute", "Metrics": [ "GroupMinSize", "GroupMaxSize" ] } ],
        "HealthCheckType" : "ELB",
        "HealthCheckGracePeriod" : 600,
        "VPCZoneIdentifier" : [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PrivSubnet3" ] }
        ],
        "Tags" : [ {
          "Key" : "Name",
          "Value" : { "Fn::Join" : [ "-", [ { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "vpc", { "Ref" : "AWS::StackName" }, "bamboo" ] ] },
          "PropagateAtLaunch" : "true"
        } ],
        "LoadBalancerNames" : [ { "Ref" : "BambooTrafficLoadBalancer" }, { "Ref" : "BambooTCPTrafficLoadBalancer" }, { "Ref" : "BambooAdminLoadBalancer" } ]
      }
    },

    "BambooTrafficLoadBalancer" : {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Scheme" : { "Fn::If" : [ "IsPublic", "internet-facing", "internal" ] },
        "Listeners" : [
          {
            "InstancePort" : "80",
            "InstanceProtocol" : "TCP",
            "LoadBalancerPort" : "80",
            "Protocol" : "TCP"
          },
          {
            "InstancePort": "80",
            "InstanceProtocol": "TCP",
            "LoadBalancerPort": "443",
            "Protocol": "SSL",
            "SSLCertificateId": "arn:aws:iam::1234567891011:server-certificate/wildcard.domain.com-2016"
          }
        ],
        "HealthCheck": {
          "Target" : "HTTP:8000/",
          "Interval" : "15",
          "Timeout" : "5",
          "HealthyThreshold" : "2",
          "UnhealthyThreshold" : "2"
        },
        "SecurityGroups" : [
          { "Ref" : "MesosPublicSecurityGroup" },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
        ],
        "Subnets": [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet3" ] }
        ]
      }
    },

    "BambooTrafficLoadBalancerDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "BambooTrafficLoadBalancer",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::GetAtt" : [ "BambooTrafficLoadBalancer", "DNSName" ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "bamboo-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60"
      }
    },

    "BambooTCPTrafficLoadBalancer" : {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Scheme" : { "Fn::If" : [ "IsPublic", "internet-facing", "internal" ] },
        "Listeners" : [
          {
            "InstancePort" : "80",
            "InstanceProtocol" : "TCP",
            "LoadBalancerPort" : "80",
            "Protocol" : "TCP"
          },
          {
            "InstancePort": "443",
            "InstanceProtocol": "TCP",
            "LoadBalancerPort": "443",
            "Protocol": "TCP"
          }
        ],
        "HealthCheck": {
          "Target" : "HTTP:8000/",
          "Interval" : "15",
          "Timeout" : "5",
          "HealthyThreshold" : "2",
          "UnhealthyThreshold" : "2"
        },
        "SecurityGroups" : [
          { "Ref" : "MesosPublicSecurityGroup" },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
        ],
        "Subnets": [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet3" ] }
        ]
      }
    },

    "BambooAdminLoadBalancer" : {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Scheme" : "internal",
        "Listeners" : [
          {
            "InstancePort" : "8000",
            "InstanceProtocol" : "HTTP",
            "LoadBalancerPort" : "8000",
            "Protocol" : "HTTP"
          },
          {
            "InstancePort" : "9000",
            "InstanceProtocol" : "HTTP",
            "LoadBalancerPort" : "9000",
            "Protocol" : "HTTP"
          }
        ],
        "HealthCheck": {
          "Target" : "HTTP:8000/",
          "Interval" : "15",
          "Timeout" : "5",
          "HealthyThreshold" : "2",
          "UnhealthyThreshold" : "2"
        },
        "SecurityGroups" : [
          { "Ref" : "MesosAdminSecurityGroup" },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, { "Ref" : "Environment" } ] }
        ],
        "Subnets": [
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet1" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet2" ] },
          { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "PubSubnet3" ] }
        ]
      }
    },

    "BambooAdminLoadBalancerDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "BambooAdminLoadBalancer",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::GetAtt" : [ "BambooAdminLoadBalancer", "DNSName" ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "bamboo-admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60"
      }
    },

    "MarathonMasterDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "AdminLoadBalancer",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::Join" : [ "", [ "admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "marathon-", { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "-", { "Fn::FindInMap" : [ "env", "access", { "Ref" : "AccessMode" } ] }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60",
        "Weight" : 0,
        "SetIdentifier" : { "Fn::Join" : [ "", [ "Marathon in ", { "Ref" : "AWS::StackName" } ] ] }
      }
    },

    "BambooAdminMasterDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "BambooAdminLoadBalancer",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::Join" : [ "", [ "bamboo-admin-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "bamboo-", { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "-", { "Fn::FindInMap" : [ "env", "access", { "Ref" : "AccessMode" } ] }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60",
        "Weight" : 0,
        "SetIdentifier" : { "Fn::Join" : [ "", [ "Bamboo in ", { "Ref" : "AWS::StackName" } ] ] }
      }
    },

    "BambooTrafficMasterDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "BambooTrafficLoadBalancer",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::Join" : [ "", [ "bamboo-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "apps-", { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "-", { "Fn::FindInMap" : [ "env", "access", { "Ref" : "AccessMode" } ] }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60",
        "Weight" : 0,
        "SetIdentifier" : { "Fn::Join" : [ "", [ "Service traffic in ", { "Ref" : "AWS::StackName" } ] ] }
      }
    },

    "BasicCloudWatchAlarm" : {
       "Type" : "AWS::CloudWatch::Alarm",
       "Properties" : {
          "ActionsEnabled" : false,
          "AlarmDescription" : "Placeholder for a CloudWatch alarm; requires no interruption to update w/in stack",
          "ComparisonOperator" : "GreaterThanThreshold",
          "EvaluationPeriods" : "2",
          "MetricName" : "CPUUtilization",
          "Namespace" : "AWS/EC2",
          "Period" : "300",
          "Statistic" : "Average",
          "Threshold" : "90"
       }
    },

    "Redis" : {
      "Type" : "AWS::CloudFormation::Stack",
      "Condition" : "IsPrivate",
      "Properties" : {
        "TemplateURL" : { "Fn::Join" : [ "", [ "https://s3.amazonaws.com/MY_BUCKET-artifacts/cloudformation/redis.json" ] ] },
        "TimeoutInMinutes" : "90",
        "Parameters" : {
          "CacheSubnetGroup" : "mesos-cache",
          "ClusterNodeType" : "cache.t2.micro",
          "ClusterSize" : 1,
          "Environment" : { "Ref" : "Environment" },
          "Platform" : "workbench",
          "RedisVersion" : "3.2.4",
          "CacheParameterGroup" : "default.redis3.2"
        }
      }
    },

    "RedisMasterDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "Redis",
      "Condition" : "IsPrivate",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::Join" : [ "", [ "redis-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "redis-", { "Fn::FindInMap" : [ "env", "type", { "Ref" : "Environment" } ] }, "-", { "Fn::FindInMap" : [ "env", "access", { "Ref" : "AccessMode" } ] }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60",
        "Weight" : 0,
        "SetIdentifier" : { "Fn::Join" : [ "", [ "Redis cluster in ", { "Ref" : "AWS::StackName" } ] ] }
      }
    },

    "RedisDNS" : {
      "Type" : "AWS::Route53::RecordSet",
      "DependsOn" : "Redis",
      "Condition" : "IsPrivate",
      "Properties" : {
        "ResourceRecords" : [
          { "Fn::GetAtt" : [ "Redis", "Outputs.RedisAddress" ] }
        ],
        "HostedZoneId" : { "Fn::FindInMap" : [ "DomainOpts", { "Ref" : "Domain" }, "zone" ] },
        "Name" : { "Fn::Join" : [ "", [ "redis-", { "Ref" : "AWS::StackName" }, ".", { "Ref": "Domain" } ] ] },
        "Type" : "CNAME",
        "TTL" : "60"
      }
    },

    "SilenceSensuClientsTeardown" : {
      "Type": "AWS::CloudFormation::CustomResource",
      "DependsOn": "BambooScalingGroup",
      "Metadata": {
        "Comment": "Depends on BambooScalingGroup, which means this should be the first thing to 'delete'"
      },
      "Properties": {
        "ServiceToken": { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "CfnSilenceSensuClients" ] },
        "Region": { "Ref": "AWS::Region" },
        "StackName": { "Ref" : "AWS::StackName" },
        "Sensu": {"Ref": "Sensu"},
        "Duration": "1800",
        "RequestType": "Delete"
      }
    }

  }
}
