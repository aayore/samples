#!/usr/bin/env python

from jinja2 import Template, Environment, FileSystemLoader, meta
import os.path, time, argparse, boto3, sys

date = time.strftime('%Y-%m-%d-%H%M')

# Parse the command line arguments
parser = argparse.ArgumentParser(description='Update a CloudFormation template template with current data')
parser.add_argument('-d','--dry-run',action='store_true',help='Create the template at /tmp/YYYY-MM-DD-hhmm.json, but don\'t upload it to S3' )
parser.add_argument('-q','--quiet',action='store_true',help='quiet mode.  Don\'t display any output other than the upload url')
parser.add_argument('template',action='store',help='Template to parse')
args = parser.parse_args()

# Make sure the provided template file exists
if not os.path.isfile(args.template):
    print 'unable to find the file you specified: ' + args.template

# Generate some standard information
s3_bucket = 'my_bucket'
s3_web_url = 'https://s3.amazonaws.com/' + s3_bucket + '/cloudformation/' + args.template.split('.')[0] + '/' + date + '.json'
s3_key = 'cloudformation/' + args.template.split('.')[0] + '/' + date + '.json'
s3_url = 's3://' + s3_bucket + '/' + s3_key

def find_newest_ami(region):
    "Determine the most recent production-ready Ubuntu 16.04 AMI in the given region"
    matching_images = []
    client = boto3.client('ec2',region_name=region)
    # We can't use a filter here because it only returns an exact match...
    images = client.describe_images(Owners=['099720109477'])['Images']
    for image in images:
        if 'Name' in image:
            if 'ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-' in image['Name']:
                matching_images.append(image)
    ami = sorted(matching_images, key=lambda k: k['CreationDate'], reverse=True)[0]['ImageId']
    return ami

## Example
# def get_logstash_s3_buckets(bucket_name):
#     paths = []
#     s3_client = boto3.client('s3')
#     for item in s3_client.list_bucket_objects(bucket_name + 'prefix'):
#         paths.append(item)
#     return paths

# Load the template from the filesystem
template_environment = Environment(loader=FileSystemLoader('.'))
template = template_environment.get_template(args.template)

# Parse the template source to look for which variables we need
template_source = template_environment.loader.get_source(template_environment, args.template)[0]
parsed_content = template_environment.parse(template_source)
template_vars = meta.find_undeclared_variables(parsed_content)

vars = {}
if 'stack_version' in template_vars:
    vars['stack_version'] = s3_web_url
if 'east_ami' in template_vars:
    vars['east_ami'] = find_newest_ami('us-east-1')
if 'west_ami' in template_vars:
    vars['west_ami'] = find_newest_ami('us-west-2')

# Write template to /tmp/
local_path = '/tmp/' + date + '.json'
template.stream(vars).dump(local_path)
if not args.quiet: print 'Template written to ' + local_path

# Upload the template (unless the --dry-run option was provided)
if not args.dry_run:
    s3 = boto3.client('s3')
    template_file = open(local_path, 'rb')
    try:
        response = s3.put_object(Bucket=s3_bucket,Key=s3_key,Body=template_file)
    except:
        print 'ERROR: Template not uploaded to S3'
        raise SystemExit
    if not args.quiet: print 'Template successfully uploaded to ' + s3_url
    print s3_web_url
